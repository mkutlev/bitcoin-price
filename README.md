# Bitcoin Price App #

### What is this repository for? ###

* Android app that shows a graphic of Bitcoin market price change in time.
* Followed MVP pattern
* Version 1.0.0

### Libraries and tools included ###

 - Support libraries
 - [RxJava](https://github.com/ReactiveX/RxJava) and [RxAndroid](https://github.com/ReactiveX/RxAndroid)
 - [Retrofit 2](http://square.github.io/retrofit/) and [OkHttp](https://github.com/square/okhttp)
 - [Dagger 2](http://google.github.io/dagger/)
 - [Butterknife](https://github.com/JakeWharton/butterknife)
 - [Espresso](https://google.github.io/android-testing-support-library/)
 - [Robolectric](http://robolectric.org/)
 - [Mockito](http://mockito.org/)

### Requirements ###

- [Android SDK](http://developer.android.com/sdk/index.html).
- [Android 7.0 (API 24)](http://developer.android.com/tools/revisions/platforms.html#7.0).
- Android SDK Tools
- Android SDK Build tools 24.0.1
- Android Support Repository