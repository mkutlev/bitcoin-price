package mk.bitcoin.test.common;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import mk.bitcoin.data.model.Market;
import mk.bitcoin.data.model.MarketValue;

/**
 * Factory class that makes instances of data models with random field values.
 * The aim of this class is to help setting up test fixtures.
 */
public class TestDataFactory {

    private static final long DAY_IN_MILLISECONDS = TimeUnit.DAYS.toMillis(1);

    private static Random random = new Random();

    public static Market newMarket(int marketValuesCount) {
        Market market = new Market();
        market.setName("Test Market");
        market.setCurrency("USD");

        double minPrice = 300.0;
        double maxPrice = 700.0;

        // Set up values array
        ArrayList<MarketValue> marketValues = new ArrayList<>();
        long nowMs = System.currentTimeMillis();
        for (int i = 0; i < marketValuesCount; i++) {
            MarketValue value = new MarketValue();
            value.setTime(nowMs - (marketValuesCount - i) * DAY_IN_MILLISECONDS);
            value.setPrice(getRandomDouble(minPrice, maxPrice));

            marketValues.add(value);
        }

        market.setValues(marketValues);

        return market;
    }

    private static double getRandomDouble(double minValue, double maxValue) {
        return random.nextDouble() * (minValue - maxValue) + minValue;
    }

}