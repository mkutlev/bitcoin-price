package mk.bitcoin.test.common.injection.component;

import javax.inject.Singleton;

import dagger.Component;
import mk.bitcoin.injection.component.ApplicationComponent;
import mk.bitcoin.test.common.injection.module.ApplicationTestModule;

@Singleton
@Component(modules = ApplicationTestModule.class)
public interface TestComponent extends ApplicationComponent {

}
