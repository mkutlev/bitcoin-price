package mk.bitcoin.test.common.injection.module;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import mk.bitcoin.BitcoinApplication;
import mk.bitcoin.data.DataManager;
import mk.bitcoin.injection.ApplicationContext;

import static org.mockito.Mockito.mock;

/**
 * Provides application-level dependencies for an app running on a testing environment
 * This allows injecting mocks if necessary.
 */
@Module
public class ApplicationTestModule {

    protected final BitcoinApplication application;

    public ApplicationTestModule(BitcoinApplication application) {
        this.application = application;
    }

    @Provides
    BitcoinApplication provideApplication() {
        return application;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return application;
    }

    /**
     * ************ MOCKS ************
     */

    @Provides
    @Singleton
    DataManager provideDataManager() {
        return mock(DataManager.class);
    }

}
