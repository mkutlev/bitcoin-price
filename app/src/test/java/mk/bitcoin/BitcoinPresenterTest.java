package mk.bitcoin;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import mk.bitcoin.data.DataManager;
import mk.bitcoin.data.model.Market;
import mk.bitcoin.test.common.TestDataFactory;
import mk.bitcoin.ui.bitcoin.BitcoinMvpView;
import mk.bitcoin.ui.bitcoin.BitcoinPresenter;
import mk.bitcoin.util.RxSchedulersOverrideRule;
import rx.Observable;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;


@RunWith(MockitoJUnitRunner.class)
public class BitcoinPresenterTest {

    @Mock
    BitcoinMvpView mockBitcoinMvpView;
    @Mock
    DataManager mockDataManager;
    private BitcoinPresenter bitcoinPresenter;

    @Rule
    public final RxSchedulersOverrideRule overrideSchedulersRule = new RxSchedulersOverrideRule();

    @Before
    public void setUp() {
        bitcoinPresenter = new BitcoinPresenter(mockDataManager);
        bitcoinPresenter.attachView(mockBitcoinMvpView);
    }

    @After
    public void tearDown() {
        bitcoinPresenter.detachView();
    }

    @Test
    public void displayMarketSuccess() {
        Market testMarket = TestDataFactory.newMarket(10);

        doReturn(Observable.just(testMarket))
                .when(mockDataManager)
                .getMarketInfo(anyInt());

        bitcoinPresenter.loadMarketInfo(false);

        verify(mockBitcoinMvpView).showProgress(true);
        verify(mockBitcoinMvpView).showMarketInfo(testMarket);
        verify(mockBitcoinMvpView).showProgress(false);
    }

    @Test
    public void displayMarketError() {
        doReturn(Observable.error(new Throwable()))
                .when(mockDataManager)
                .getMarketInfo(anyInt());

        bitcoinPresenter.loadMarketInfo(false);

        verify(mockBitcoinMvpView).showProgress(true);
        verify(mockBitcoinMvpView).showError();
        verify(mockBitcoinMvpView).showProgress(false);

        verify(mockBitcoinMvpView, never()).showMarketInfo(any(Market.class));
    }

}