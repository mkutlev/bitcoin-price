package mk.bitcoin;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import mk.bitcoin.data.DataManager;
import mk.bitcoin.data.model.Market;
import mk.bitcoin.data.remote.ApiService;
import mk.bitcoin.test.common.TestDataFactory;
import rx.Observable;
import rx.observers.TestSubscriber;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doReturn;

/**
 * This test class performs local unit tests without dependencies on the Android framework
 */
@RunWith(MockitoJUnitRunner.class)
public class DataManagerTest {

    @Mock
    ApiService mockApiService;

    DataManager dataManager;

    @Before
    public void setUp() {
        dataManager = new DataManager(mockApiService);
    }

    @Test
    public void getMarket() {
        Market testMarket = TestDataFactory.newMarket(10);
        doReturn(Observable.just(testMarket))
                .when(mockApiService)
                .getMarketInfo(anyString());

        TestSubscriber<Market> testSubscriber = new TestSubscriber<>();
        dataManager.getMarketInfo(10).subscribe(testSubscriber);
        testSubscriber.assertCompleted();
        testSubscriber.assertValueCount(1);
        testSubscriber.assertValue(testMarket);
    }

}
