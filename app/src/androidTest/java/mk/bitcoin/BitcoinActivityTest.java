package mk.bitcoin;

import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;

import mk.bitcoin.test.common.TestComponentRule;
import mk.bitcoin.test.common.TestDataFactory;
import mk.bitcoin.ui.bitcoin.BitcoinActivity;
import rx.Observable;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.doReturn;

@RunWith(AndroidJUnit4.class)
public class BitcoinActivityTest {

    public final TestComponentRule component =
            new TestComponentRule(InstrumentationRegistry.getTargetContext());

    public final ActivityTestRule<BitcoinActivity> main =
            new ActivityTestRule<>(BitcoinActivity.class, false, false);

    // TestComponentRule needs to go first so we make sure the ApplicationTestComponent is set
    // in the Application before any Activity is launched.
    @Rule
    public TestRule chain = RuleChain.outerRule(component).around(main);

    @Before
    public void setUp() {
        doReturn(Observable.just(TestDataFactory.newMarket(10)))
                .when(component.getMockDataManager())
                .getMarketInfo(anyInt());
    }

    @Test
    public void checkViewsDisplay() {
        main.launchActivity(null);

        onView(withText(R.string.bitcoin_info_title)).check(matches(isDisplayed()));

        onView(withId(R.id.graph_view))
                .check(matches(isDisplayed()));
    }

}