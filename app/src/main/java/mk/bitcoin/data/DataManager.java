package mk.bitcoin.data;

import javax.inject.Inject;
import javax.inject.Singleton;

import mk.bitcoin.data.model.Market;
import mk.bitcoin.data.remote.ApiService;
import rx.Observable;

@Singleton
public class DataManager {

    private static final String TIMESPAN_DAYS_SUFFIX = "days";

    private final ApiService apiService;

    @Inject
    public DataManager(ApiService apiService) {
        this.apiService = apiService;
    }

    public Observable<Market> getMarketInfo(int days) {
        return apiService.getMarketInfo(String.valueOf(days) + TIMESPAN_DAYS_SUFFIX);
    }

}
