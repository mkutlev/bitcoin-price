package mk.bitcoin.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Market {

    private String name;

    @SerializedName("unit")
    private String currency;

    private double minPrice = -1.0;
    private double maxPrice = -1.0;

    private ArrayList<MarketValue> values;

    public Market() {
    }

    public double getLowestPrice() {
        if (minPrice < 0) {
            calcMaxMinPrice();
        }

        return minPrice;
    }

    public double getHighestPrice() {
        if (maxPrice < 0) {
            calcMaxMinPrice();
        }

        return maxPrice;
    }

    /**
     * Returns earliest time of market values
     */
    public long getMinTime() {
        if (values == null || values.isEmpty()) {
            return 0;
        }

        return values.get(0).getTime();
    }

    /**
     * Returns latest time of market values
     */
    public long getMaxTime() {
        if (values == null || values.isEmpty()) {
            return 0;
        }

        return values.get(values.size() - 1).getTime();
    }

    private void calcMaxMinPrice() {
        if (values == null || values.isEmpty()) {
            minPrice = 0.0d;
            maxPrice = 0.0d;
            return;
        }

        minPrice = values.get(0).getPrice();
        maxPrice = minPrice;

        for (MarketValue value : values) {
            if (value.getPrice() > maxPrice) {
                maxPrice = value.getPrice();
            } else if (value.getPrice() < minPrice) {
                minPrice = value.getPrice();
            }
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public ArrayList<MarketValue> getValues() {
        return values;
    }

    public void setValues(ArrayList<MarketValue> values) {
        this.values = values;
    }

}
