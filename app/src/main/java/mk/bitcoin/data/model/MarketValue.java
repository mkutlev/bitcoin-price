package mk.bitcoin.data.model;

import com.google.gson.annotations.SerializedName;

public class MarketValue {

    /**
     * Time as Unix timestamp
     */
    @SerializedName("x")
    private long time;

    @SerializedName("y")
    private double price;

    public MarketValue() {
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

}
