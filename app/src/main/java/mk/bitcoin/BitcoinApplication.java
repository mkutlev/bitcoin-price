package mk.bitcoin;

import android.app.Application;
import android.content.Context;

import mk.bitcoin.injection.component.ApplicationComponent;
import mk.bitcoin.injection.component.DaggerApplicationComponent;
import mk.bitcoin.injection.module.ApplicationModule;

public class BitcoinApplication extends Application {

    ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public static BitcoinApplication get(Context context) {
        return (BitcoinApplication) context.getApplicationContext();
    }

    public ApplicationComponent getComponent() {
        if (applicationComponent == null) {
            applicationComponent = DaggerApplicationComponent.builder()
                    .applicationModule(new ApplicationModule(this))
                    .build();
        }
        return applicationComponent;
    }

    // Needed to replace the component with a test specific one
    public void setComponent(ApplicationComponent applicationComponent) {
        this.applicationComponent = applicationComponent;
    }

}
