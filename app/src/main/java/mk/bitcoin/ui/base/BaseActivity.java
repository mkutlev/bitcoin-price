package mk.bitcoin.ui.base;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import mk.bitcoin.BitcoinApplication;
import mk.bitcoin.injection.component.ActivityComponent;
import mk.bitcoin.injection.component.DaggerActivityComponent;
import mk.bitcoin.injection.module.ActivityModule;

public class BaseActivity extends AppCompatActivity {

    private ActivityComponent activityComponent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public ActivityComponent getActivityComponent() {
        if (activityComponent == null) {

            activityComponent = DaggerActivityComponent.builder()
                    .activityModule(new ActivityModule(this))
                    .applicationComponent(BitcoinApplication.get(this).getComponent())
                    .build();
        }
        return activityComponent;
    }

}
