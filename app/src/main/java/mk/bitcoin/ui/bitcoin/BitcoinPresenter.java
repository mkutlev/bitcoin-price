package mk.bitcoin.ui.bitcoin;

import javax.inject.Inject;

import mk.bitcoin.data.DataManager;
import mk.bitcoin.data.model.Market;
import mk.bitcoin.ui.base.BasePresenter;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

public class BitcoinPresenter extends BasePresenter<BitcoinMvpView> {

    private static final String TAG = BitcoinPresenter.class.getSimpleName();

    private static final int DAYS_TO_SHOW = 30;

    private DataManager dataManager;

    private BitcoinMvpView mvpView;

    private Market cachedMarket;

    public Subscription subscription;

    @Inject
    public BitcoinPresenter(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    @Override
    public void attachView(BitcoinMvpView mvpView) {
        super.attachView(mvpView);

        this.mvpView = mvpView;
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    /**
     * Load the Market information
     *
     * @param allowMemoryCacheVersion if true a cached version will be returned from memory
     */
    public void loadMarketInfo(boolean allowMemoryCacheVersion) {
        mvpView.showProgress(true);
        if (subscription != null) {
            subscription.unsubscribe();
        }

        subscription = getMarketObservable(allowMemoryCacheVersion)
                .subscribe(new Subscriber<Market>() {
                    @Override
                    public void onCompleted() {
                        Timber.d("Market info loaded.");

                        mvpView.showProgress(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e, "There was an error retrieving the market information ");

                        mvpView.showProgress(false);

                        mvpView.showError();
                    }

                    @Override
                    public void onNext(Market market) {
                        cachedMarket = market;

                        if (market.getValues() == null || market.getValues().isEmpty()) {
                            mvpView.showEmptyMessage();
                        } else {
                            mvpView.showMarketInfo(market);
                        }
                    }
                });
    }

    private Observable<Market> getMarketObservable(boolean allowMemoryCacheVersion) {
        if (allowMemoryCacheVersion && cachedMarket != null) {
            return Observable.just(cachedMarket);
        } else {
            return dataManager.getMarketInfo(DAYS_TO_SHOW)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io());
        }
    }

}
