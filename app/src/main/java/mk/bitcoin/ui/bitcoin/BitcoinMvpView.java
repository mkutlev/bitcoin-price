package mk.bitcoin.ui.bitcoin;

import mk.bitcoin.data.model.Market;
import mk.bitcoin.ui.base.MvpView;

public interface BitcoinMvpView extends MvpView {

    void showProgress(boolean show);

    void showMarketInfo(Market market);

    void showEmptyMessage();

    void showError();

}
