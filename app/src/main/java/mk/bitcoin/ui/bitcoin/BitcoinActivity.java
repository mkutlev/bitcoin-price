package mk.bitcoin.ui.bitcoin;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.View;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import mk.bitcoin.R;
import mk.bitcoin.data.model.Market;
import mk.bitcoin.ui.base.BaseActivity;
import mk.bitcoin.ui.graph.GraphView;
import mk.bitcoin.util.DialogFactory;
import mk.bitcoin.util.NetworkConnectionUtil;

public class BitcoinActivity extends BaseActivity implements BitcoinMvpView {

    @Inject
    BitcoinPresenter bitcoinPresenter;

    @BindView(R.id.graph_view)
    GraphView graphView;

    @BindView(R.id.no_data_message)
    TextView noDataMsgView;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Set up activity theme
        // Make sure this is before calling super.onCreate
        setTheme(R.style.AppTheme);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bitcoin_info);

        getActivityComponent().inject(this);
        ButterKnife.bind(this);

        bitcoinPresenter.attachView(this);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(R.string.bitcoin_info_title);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        bitcoinPresenter.loadMarketInfo(true);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        bitcoinPresenter.detachView();
    }

    @Override
    public void showProgress(boolean show) {
        if (show) {
            progressDialog = DialogFactory.createProgressDialog(this, R.string.dialog_loading_message);
            progressDialog.show();
        } else {
            if (progressDialog != null) {
                progressDialog.dismiss();
                progressDialog = null;
            }
        }
    }

    @Override
    public void showMarketInfo(Market market) {
        noDataMsgView.setVisibility(View.GONE);

        graphView.showMarketInfo(market);
    }

    @Override
    public void showEmptyMessage() {
        noDataMsgView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showError() {
        if (!NetworkConnectionUtil.isNetworkAvailable(this)) {
            DialogFactory.createGenericErrorDialog(this, R.string.noInternetMessage).show();
            return;
        }

        DialogFactory.createGenericErrorDialog(this, R.string.error_could_not_load_market).show();
    }

}
