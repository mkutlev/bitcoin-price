package mk.bitcoin.ui.graph;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.os.ParcelableCompat;
import android.support.v4.os.ParcelableCompatCreatorCallbacks;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.OverScroller;

/**
 * Base class to be used for views that handle the base
 * gestures touch, double tap, fling, zoom and pan.
 * <p>
 * Base on "Using touch gestures" - https://developer.android.com/training/gestures/index.html
 */
public abstract class InteractiveView extends View {

    /**
     * Initial fling velocity for pan operations, in screen widths (or heights) per second.
     *
     * @see #panLeft()
     * @see #panRight()
     * @see #panUp()
     * @see #panDown()
     */
    private static final float PAN_VELOCITY_FACTOR = 2f;

    /**
     * The scaling factor for a single zoom 'step'.
     *
     * @see #zoomIn()
     * @see #zoomOut()
     */
    private static final float ZOOM_AMOUNT = 0.25f;

    /**
     * The current viewport. This rectangle represents the currently visible chart domain
     * and range. The currently visible chart X values are from this rectangle's left to its right.
     * The currently visible chart Y values are from this rectangle's top to its bottom.
     * <p>
     * Note that this rectangle's top is actually the smaller Y value, and its bottom is the larger
     * Y value. Since the chart is drawn onscreen in such a way that chart Y values increase
     * towards the top of the screen (decreasing pixel Y positions), this rectangle's "top" is drawn
     * above this rectangle's "bottom" value.
     *
     * @see #contentRect
     */
    protected RectF currentViewport = new RectF();

    /**
     * The current destination rectangle (in pixel coordinates) into which the chart data should
     * be drawn. Chart labels are drawn outside this area.
     *
     * @see #currentViewport
     */
    protected Rect contentRect = new Rect();

    // State objects and values related to gesture tracking.
    private ScaleGestureDetector scaleGestureDetector;
    private GestureDetectorCompat gestureDetector;
    private OverScroller scroller;
    private Zoomer zoomer;
    private PointF zoomFocalPoint = new PointF();
    private RectF scrollerStartViewport = new RectF(); // Used only for zooms and flings.

    private Point surfaceSizeBuffer = new Point();

    private float axisXMin = 0f;
    private float axisXMax = 0f;
    private float axisYMin = 0f;
    private float axisYMax = 0f;

    public InteractiveView(Context context) {
        this(context, null, 0);
    }

    public InteractiveView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public InteractiveView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        // Sets up interactions
        scaleGestureDetector = new ScaleGestureDetector(context, scaleGestureListener);
        gestureDetector = new GestureDetectorCompat(context, gestureListener);

        scroller = new OverScroller(context);
        zoomer = new Zoomer(context);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        contentRect.set(
                getPaddingLeft(),
                getPaddingTop(),
                getWidth() - getPaddingRight(),
                getHeight() - getPaddingBottom());
    }

    protected void setAxisMinMaxValues(float axisXMin, float axisYMin, float axisXMax, float axisYMax) {
        this.axisXMin = axisXMin;
        this.axisXMax = axisXMax;

        this.axisYMin = axisYMin;
        this.axisYMax = axisYMax;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //     Methods and objects related to drawing
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    /**
     * Computes the pixel offset for the given X chart value. This may be outside the view bounds.
     */
    protected float getDrawX(float x) {
        return contentRect.left
                + contentRect.width()
                * (x - currentViewport.left) / currentViewport.width();
    }

    /**
     * Computes the pixel offset for the given Y chart value. This may be outside the view bounds.
     */
    protected float getDrawY(float y) {
        return contentRect.bottom
                - contentRect.height()
                * (y - currentViewport.top) / currentViewport.height();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //     Methods and objects related to gesture handling
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Finds the chart point (i.e. within the chart's domain and range) represented by the
     * given pixel coordinates, if that pixel is within the chart region described by
     * {@link #contentRect}. If the point is found, the "dest" argument is set to the point and
     * this function returns true. Otherwise, this function returns false and "dest" is unchanged.
     */
    private boolean hitTest(float x, float y, PointF dest) {
        if (!contentRect.contains((int) x, (int) y)) {
            return false;
        }

        dest.set(
                currentViewport.left
                        + currentViewport.width()
                        * (x - contentRect.left) / contentRect.width(),
                currentViewport.top
                        + currentViewport.height()
                        * (y - contentRect.bottom) / -contentRect.height());
        return true;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        boolean retVal = scaleGestureDetector.onTouchEvent(event);
        retVal = gestureDetector.onTouchEvent(event) || retVal;
        return retVal || super.onTouchEvent(event);
    }

    /**
     * The scale listener, used for handling multi-finger scale gestures.
     */
    private final ScaleGestureDetector.OnScaleGestureListener scaleGestureListener
            = new ScaleGestureDetector.SimpleOnScaleGestureListener() {
        /**
         * This is the active focal point in terms of the viewport. Could be a local
         * variable but kept here to minimize per-frame allocations.
         */
        private PointF viewportFocus = new PointF();
        private float lastSpanX;
        private float lastSpanY;

        @Override
        public boolean onScaleBegin(ScaleGestureDetector scaleGestureDetector) {
            lastSpanX = scaleGestureDetector.getCurrentSpanX();
            lastSpanY = scaleGestureDetector.getCurrentSpanY();
            return true;
        }

        @Override
        public boolean onScale(ScaleGestureDetector scaleGestureDetector) {
            float spanX = scaleGestureDetector.getCurrentSpanX();
            float spanY = scaleGestureDetector.getCurrentSpanY();

            float newWidth = lastSpanX / spanX * currentViewport.width();
            float newHeight = lastSpanY / spanY * currentViewport.height();

            float focusX = scaleGestureDetector.getFocusX();
            float focusY = scaleGestureDetector.getFocusY();
            hitTest(focusX, focusY, viewportFocus);

            currentViewport.set(
                    viewportFocus.x
                            - newWidth * (focusX - contentRect.left)
                            / contentRect.width(),
                    viewportFocus.y
                            - newHeight * (contentRect.bottom - focusY)
                            / contentRect.height(),
                    0,
                    0);
            currentViewport.right = currentViewport.left + newWidth;
            currentViewport.bottom = currentViewport.top + newHeight;
            constrainViewport();
            ViewCompat.postInvalidateOnAnimation(InteractiveView.this);

            lastSpanX = spanX;
            lastSpanY = spanY;
            return true;
        }
    };

    /**
     * Ensures that current viewport is inside the viewport extremes.
     */
    private void constrainViewport() {
        currentViewport.left = Math.max(axisXMin, currentViewport.left);
        currentViewport.top = Math.max(axisYMin, currentViewport.top);
        currentViewport.bottom = Math.max(Math.nextUp(currentViewport.top),
                Math.min(axisYMax, currentViewport.bottom));
        currentViewport.right = Math.max(Math.nextUp(currentViewport.left),
                Math.min(axisXMax, currentViewport.right));
    }

    /**
     * The gesture listener, used for handling simple gestures such as double touches, scrolls,
     * and flings.
     */
    private final GestureDetector.SimpleOnGestureListener gestureListener
            = new GestureDetector.SimpleOnGestureListener() {
        @Override
        public boolean onDown(MotionEvent e) {
            scrollerStartViewport.set(currentViewport);
            scroller.forceFinished(true);
            ViewCompat.postInvalidateOnAnimation(InteractiveView.this);
            return true;
        }

        @Override
        public boolean onDoubleTap(MotionEvent e) {
            zoomer.forceFinished(true);
            if (hitTest(e.getX(), e.getY(), zoomFocalPoint)) {
                zoomer.startZoom(ZOOM_AMOUNT);
            }
            ViewCompat.postInvalidateOnAnimation(InteractiveView.this);
            return true;
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            // Scrolling uses math based on the viewport (as opposed to math using pixels).
            /**
             * Pixel offset is the offset in screen pixels, while viewport offset is the
             * offset within the current viewport. For additional information on surface sizes
             * and pixel offsets, see the docs for {@link computeScrollSurfaceSize()}. For
             * additional information about the viewport, see the comments for
             * {@link currentViewport}.
             */
            float viewportOffsetX = distanceX * currentViewport.width() / contentRect.width();
            float viewportOffsetY = -distanceY * currentViewport.height() / contentRect.height();
            computeScrollSurfaceSize(surfaceSizeBuffer);

            setViewportBottomLeft(
                    currentViewport.left + viewportOffsetX,
                    currentViewport.bottom + viewportOffsetY);

            return true;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            fling((int) -velocityX, (int) -velocityY);
            return true;
        }
    };

    private void fling(int velocityX, int velocityY) {
        // Flings use math in pixels (as opposed to math based on the viewport).
        computeScrollSurfaceSize(surfaceSizeBuffer);
        scrollerStartViewport.set(currentViewport);
        int startX = (int) (surfaceSizeBuffer.x * (scrollerStartViewport.left - axisXMin) / (
                axisXMax - axisXMin));
        int startY = (int) (surfaceSizeBuffer.y * (axisYMax - scrollerStartViewport.bottom) / (
                axisYMax - axisYMin));
        scroller.forceFinished(true);
        scroller.fling(
                startX,
                startY,
                velocityX,
                velocityY,
                0, surfaceSizeBuffer.x - contentRect.width(),
                0, surfaceSizeBuffer.y - contentRect.height(),
                contentRect.width() / 2,
                contentRect.height() / 2);
        ViewCompat.postInvalidateOnAnimation(this);
    }

    /**
     * Computes the current scrollable surface size, in pixels. For example, if the entire chart
     * area is visible, this is simply the current size of {@link #contentRect}. If the chart
     * is zoomed in 200% in both directions, the returned size will be twice as large horizontally
     * and vertically.
     */
    private void computeScrollSurfaceSize(Point out) {
        out.set(
                (int) (contentRect.width() * (axisXMax - axisXMin)
                        / currentViewport.width()),
                (int) (contentRect.height() * (axisYMax - axisYMin)
                        / currentViewport.height()));
    }

    @Override
    public void computeScroll() {
        super.computeScroll();

        boolean needsInvalidate = false;

        if (scroller.computeScrollOffset()) {
            // The scroller isn't finished, meaning a fling or programmatic pan operation is
            // currently active.

            computeScrollSurfaceSize(surfaceSizeBuffer);
            int currX = scroller.getCurrX();
            int currY = scroller.getCurrY();

            boolean canScrollX = (currentViewport.left > axisXMin
                    || currentViewport.right < axisXMax);
            boolean canScrollY = (currentViewport.top > axisYMin
                    || currentViewport.bottom < axisYMax);

            if (canScrollX && currX < 0) {
                needsInvalidate = true;
            } else if (canScrollX && currX > (surfaceSizeBuffer.x - contentRect.width())) {
                needsInvalidate = true;
            }

            if (canScrollY && currY < 0) {
                needsInvalidate = true;
            } else if (canScrollY && currY > (surfaceSizeBuffer.y - contentRect.height())) {
                needsInvalidate = true;
            }

            float currXRange = axisXMin + (axisXMax - axisXMin)
                    * currX / surfaceSizeBuffer.x;
            float currYRange = axisYMax - (axisYMax - axisYMin)
                    * currY / surfaceSizeBuffer.y;
            setViewportBottomLeft(currXRange, currYRange);
        }

        if (zoomer.computeZoom()) {
            // Performs the zoom since a zoom is in progress (either programmatically or via
            // double-touch).
            float newWidth = (1f - zoomer.getCurrZoom()) * scrollerStartViewport.width();
            float newHeight = (1f - zoomer.getCurrZoom()) * scrollerStartViewport.height();
            float pointWithinViewportX = (zoomFocalPoint.x - scrollerStartViewport.left)
                    / scrollerStartViewport.width();
            float pointWithinViewportY = (zoomFocalPoint.y - scrollerStartViewport.top)
                    / scrollerStartViewport.height();
            currentViewport.set(
                    zoomFocalPoint.x - newWidth * pointWithinViewportX,
                    zoomFocalPoint.y - newHeight * pointWithinViewportY,
                    zoomFocalPoint.x + newWidth * (1 - pointWithinViewportX),
                    zoomFocalPoint.y + newHeight * (1 - pointWithinViewportY));
            constrainViewport();
            needsInvalidate = true;
        }

        if (needsInvalidate) {
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }

    /**
     * Sets the current viewport (defined by {@link #currentViewport}) to the given
     * X and Y positions. Note that the Y value represents the topmost pixel position, and thus
     * the bottom of the {@link #currentViewport} rectangle. For more details on why top and
     * bottom are flipped, see {@link #currentViewport}.
     */
    private void setViewportBottomLeft(float x, float y) {
        /**
         * Constrains within the scroll range. The scroll range is simply the viewport extremes
         * (AXIS_X_MAX, etc.) minus the viewport size. For example, if the extrema were 0 and 10,
         * and the viewport size was 2, the scroll range would be 0 to 8.
         */

        float curWidth = currentViewport.width();
        float curHeight = currentViewport.height();

        x = Math.max(axisXMin, Math.min(x, axisXMax - curWidth));
        y = Math.max(axisYMin + curHeight, Math.min(y, axisYMax));

        currentViewport.set(x, y - curHeight, x + curWidth, y);
        ViewCompat.postInvalidateOnAnimation(this);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //     Methods for programmatically changing the viewport
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the current viewport (visible extremes for the chart domain and range.)
     */
    public RectF getCurrentViewport() {
        return new RectF(currentViewport);
    }

    /**
     * Sets the chart's current viewport.
     *
     * @see #getCurrentViewport()
     */
    public void setCurrentViewport(RectF viewport) {
        currentViewport = viewport;
        constrainViewport();
        ViewCompat.postInvalidateOnAnimation(this);
    }

    /**
     * Smoothly zooms the chart in one step.
     */
    public void zoomIn() {
        scrollerStartViewport.set(currentViewport);
        zoomer.forceFinished(true);
        zoomer.startZoom(ZOOM_AMOUNT);
        zoomFocalPoint.set(
                (currentViewport.right + currentViewport.left) / 2,
                (currentViewport.bottom + currentViewport.top) / 2);
        ViewCompat.postInvalidateOnAnimation(this);
    }

    /**
     * Smoothly zooms the chart out one step.
     */
    public void zoomOut() {
        scrollerStartViewport.set(currentViewport);
        zoomer.forceFinished(true);
        zoomer.startZoom(-ZOOM_AMOUNT);
        zoomFocalPoint.set(
                (currentViewport.right + currentViewport.left) / 2,
                (currentViewport.bottom + currentViewport.top) / 2);
        ViewCompat.postInvalidateOnAnimation(this);
    }

    /**
     * Smoothly pans the chart left one step.
     */
    public void panLeft() {
        fling((int) (-PAN_VELOCITY_FACTOR * getWidth()), 0);
    }

    /**
     * Smoothly pans the chart right one step.
     */
    public void panRight() {
        fling((int) (PAN_VELOCITY_FACTOR * getWidth()), 0);
    }

    /**
     * Smoothly pans the chart up one step.
     */
    public void panUp() {
        fling(0, (int) (-PAN_VELOCITY_FACTOR * getHeight()));
    }

    /**
     * Smoothly pans the chart down one step.
     */
    public void panDown() {
        fling(0, (int) (PAN_VELOCITY_FACTOR * getHeight()));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //     Methods and classes related to view state persistence.
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();
        SavedState ss = new SavedState(superState);
        ss.viewport = currentViewport;
        return ss;
    }

    @Override
    public void onRestoreInstanceState(Parcelable state) {
        if (!(state instanceof SavedState)) {
            super.onRestoreInstanceState(state);
            return;
        }

        SavedState ss = (SavedState) state;
        super.onRestoreInstanceState(ss.getSuperState());

        currentViewport = ss.viewport;
    }

    /**
     * Persistent state that is saved by GraphView.
     */
    public static class SavedState extends BaseSavedState {
        private RectF viewport;

        public SavedState(Parcelable superState) {
            super(superState);
        }

        @Override
        public void writeToParcel(Parcel out, int flags) {
            super.writeToParcel(out, flags);
            out.writeFloat(viewport.left);
            out.writeFloat(viewport.top);
            out.writeFloat(viewport.right);
            out.writeFloat(viewport.bottom);
        }

        @Override
        public String toString() {
            return "InteractiveView.SavedState{"
                    + Integer.toHexString(System.identityHashCode(this))
                    + " viewport=" + viewport.toString() + "}";
        }

        public static final Creator<SavedState> CREATOR
                = ParcelableCompat.newCreator(new ParcelableCompatCreatorCallbacks<SavedState>() {
            @Override
            public SavedState createFromParcel(Parcel in, ClassLoader loader) {
                return new SavedState(in);
            }

            @Override
            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        });

        SavedState(Parcel in) {
            super(in);
            viewport = new RectF(in.readFloat(), in.readFloat(), in.readFloat(), in.readFloat());
        }
    }

}
