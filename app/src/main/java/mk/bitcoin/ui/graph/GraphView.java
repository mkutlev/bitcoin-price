package mk.bitcoin.ui.graph;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import mk.bitcoin.R;
import mk.bitcoin.data.model.Market;
import mk.bitcoin.data.model.MarketValue;
import timber.log.Timber;

public class GraphView extends InteractiveView {

    private static final String DATE_FORMAT = "dd.MM";

    private static final float Y_AXIS_PADDING = 3.0f;

    // Current attribute values and Paints.
    private float labelTextSize;
    private int labelSeparation;
    private int labelTextColor;
    private Paint labelTextPaint;
    private int maxPriceLabelWidth;
    private int maxTimeLabelWidth;
    private int labelHeight;
    private float gridThickness;
    private int gridColor;
    private Paint gridPaint;
    private float axisThickness;
    private int axisColor;
    private Paint axisPaint;
    private float dataThickness;
    private int dataColor;
    private Paint dataPaint;

    // Buffers for storing current X and Y stops. See the computeAxisStops method for more details.
    private final AxisStops xStopsBuffer = new AxisStops();
    private final AxisStops yStopsBuffer = new AxisStops();

    // Buffers used during drawing. These are defined as fields to avoid allocation during
    // draw calls.
    private float[] axisXPositionsBuffer = new float[]{};
    private float[] axisYPositionsBuffer = new float[]{};
    private float[] axisXLinesBuffer = new float[]{};
    private float[] axisYLinesBuffer = new float[]{};
    private float[] graphLinesBuffer = new float[]{};
    private final char[] labelBuffer = new char[100];

    private Market market;

    private SimpleDateFormat dateFormat;

    public GraphView(Context context) {
        this(context, null, 0);
    }

    public GraphView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public GraphView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs, R.styleable.GraphView, defStyle, defStyle);

        try {
            labelTextColor = a.getColor(
                    R.styleable.GraphView_labelTextColor, labelTextColor);
            labelTextSize = a.getDimension(
                    R.styleable.GraphView_labelTextSize, labelTextSize);
            labelSeparation = a.getDimensionPixelSize(
                    R.styleable.GraphView_labelSeparation, labelSeparation);

            gridThickness = a.getDimension(
                    R.styleable.GraphView_gridThickness, gridThickness);
            gridColor = a.getColor(
                    R.styleable.GraphView_gridColor, gridColor);

            axisThickness = a.getDimension(
                    R.styleable.GraphView_axisThickness, axisThickness);
            axisColor = a.getColor(
                    R.styleable.GraphView_axisColor, axisColor);

            dataThickness = a.getDimension(
                    R.styleable.GraphView_dataThickness, dataThickness);
            dataColor = a.getColor(
                    R.styleable.GraphView_dataColor, dataColor);
        } finally {
            a.recycle();
        }

        initPaints();

        dateFormat = new SimpleDateFormat(DATE_FORMAT, Locale.US);
    }

    /**
     * (Re)initializes {@link Paint} objects based on current attribute values.
     */
    private void initPaints() {
        labelTextPaint = new Paint();
        labelTextPaint.setAntiAlias(true);
        labelTextPaint.setTextSize(labelTextSize);
        labelTextPaint.setColor(labelTextColor);
        labelHeight = (int) Math.abs(labelTextPaint.getFontMetrics().top);
        maxPriceLabelWidth = (int) labelTextPaint.measureText("0000");
        maxTimeLabelWidth = (int) labelTextPaint.measureText("00.00");

        gridPaint = new Paint();
        gridPaint.setStrokeWidth(gridThickness);
        gridPaint.setColor(gridColor);
        gridPaint.setStyle(Paint.Style.STROKE);

        axisPaint = new Paint();
        axisPaint.setStrokeWidth(axisThickness);
        axisPaint.setColor(axisColor);
        axisPaint.setStyle(Paint.Style.STROKE);

        dataPaint = new Paint();
        dataPaint.setStrokeWidth(dataThickness);
        dataPaint.setColor(dataColor);
        dataPaint.setStyle(Paint.Style.STROKE);
        dataPaint.setAntiAlias(true);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        contentRect.set(
                getPaddingLeft() + maxPriceLabelWidth + labelSeparation,
                getPaddingTop(),
                getWidth() - getPaddingRight(),
                getHeight() - getPaddingBottom() - labelHeight - labelSeparation);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int minChartSize = getResources().getDimensionPixelSize(R.dimen.min_chart_size);
        setMeasuredDimension(
                Math.max(getSuggestedMinimumWidth(),
                        resolveSize(minChartSize + getPaddingLeft() + maxPriceLabelWidth
                                        + labelSeparation + getPaddingRight(),
                                widthMeasureSpec)),
                Math.max(getSuggestedMinimumHeight(),
                        resolveSize(minChartSize + getPaddingTop() + labelHeight
                                        + labelSeparation + getPaddingBottom(),
                                heightMeasureSpec)));
    }

    public void showMarketInfo(Market market) {
        this.market = market;

        setUpViewportValues();

        graphLinesBuffer = new float[(market.getValues().size() + 1) * 4];

        invalidate();
    }

    private void setUpViewportValues() {
        float axisXMin = market.getMinTime();
        float axisXMax = market.getMaxTime();

        float axisYMin = (float) market.getLowestPrice();
        float axisYMax = (float) market.getHighestPrice();

        if (axisYMax - axisYMin > Y_AXIS_PADDING * 10) {
            axisYMin = axisYMin - Y_AXIS_PADDING;
            axisYMax = axisYMax + Y_AXIS_PADDING;
        }

        setAxisMinMaxValues(axisXMin, axisYMin, axisXMax, axisYMax);

        currentViewport.set(axisXMin, axisYMin, axisXMax, axisYMax);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //     Methods and objects related to drawing
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (market == null) {
            Timber.d("Market info not loaded yet!");
            return;
        }

        // Draws axes and text labels
        drawAxes(canvas);

        drawCurrency(canvas);

        // Clips the next few drawing operations to the content area
        int clipRestoreCount = canvas.save();
        canvas.clipRect(contentRect);

        // Draws the graph
        drawGraphUnclipped(canvas);

        // Removes clipping rectangle
        canvas.restoreToCount(clipRestoreCount);

        // Draws chart container
        canvas.drawRect(contentRect, axisPaint);
    }

    /**
     * Draws the currency label onto the canvas.
     */
    private void drawCurrency(Canvas canvas) {
        canvas.drawText(
                market.getCurrency(),
                contentRect.left - labelSeparation,
                contentRect.top,
                labelTextPaint);
    }

    /**
     * Draws the chart axes and labels onto the canvas.
     */
    private void drawAxes(Canvas canvas) {
        // Computes axis stops (in terms of numerical value and position on screen)
        int i;

        computeAxisStops(
                currentViewport.left,
                currentViewport.right,
                contentRect.width() / maxTimeLabelWidth / 2,
                xStopsBuffer);
        computeAxisStops(
                currentViewport.top,
                currentViewport.bottom,
                contentRect.height() / labelHeight / 2,
                yStopsBuffer);

        // Avoid unnecessary allocations during drawing. Re-use allocated
        // arrays and only reallocate if the number of stops grows.
        if (axisXPositionsBuffer.length < xStopsBuffer.numStops) {
            axisXPositionsBuffer = new float[xStopsBuffer.numStops];
        }
        if (axisYPositionsBuffer.length < yStopsBuffer.numStops) {
            axisYPositionsBuffer = new float[yStopsBuffer.numStops];
        }
        if (axisXLinesBuffer.length < xStopsBuffer.numStops * 4) {
            axisXLinesBuffer = new float[xStopsBuffer.numStops * 4];
        }
        if (axisYLinesBuffer.length < yStopsBuffer.numStops * 4) {
            axisYLinesBuffer = new float[yStopsBuffer.numStops * 4];
        }

        // Compute positions
        for (i = 0; i < xStopsBuffer.numStops; i++) {
            axisXPositionsBuffer[i] = getDrawX(xStopsBuffer.stops[i]);
        }
        for (i = 0; i < yStopsBuffer.numStops; i++) {
            axisYPositionsBuffer[i] = getDrawY(yStopsBuffer.stops[i]);
        }

        // Draws grid lines using drawLines (faster than individual drawLine calls)
        for (i = 0; i < xStopsBuffer.numStops; i++) {
            axisXLinesBuffer[i * 4 + 0] = (float) Math.floor(axisXPositionsBuffer[i]);
            axisXLinesBuffer[i * 4 + 1] = contentRect.top;
            axisXLinesBuffer[i * 4 + 2] = (float) Math.floor(axisXPositionsBuffer[i]);
            axisXLinesBuffer[i * 4 + 3] = contentRect.bottom;
        }
        canvas.drawLines(axisXLinesBuffer, 0, xStopsBuffer.numStops * 4, gridPaint);

        for (i = 0; i < yStopsBuffer.numStops; i++) {
            axisYLinesBuffer[i * 4 + 0] = contentRect.left;
            axisYLinesBuffer[i * 4 + 1] = (float) Math.floor(axisYPositionsBuffer[i]);
            axisYLinesBuffer[i * 4 + 2] = contentRect.right;
            axisYLinesBuffer[i * 4 + 3] = (float) Math.floor(axisYPositionsBuffer[i]);
        }
        canvas.drawLines(axisYLinesBuffer, 0, yStopsBuffer.numStops * 4, gridPaint);

        // Draws X labels
        int labelOffset;
        int labelLength;
        labelTextPaint.setTextAlign(Paint.Align.CENTER);

        for (i = 0; i < xStopsBuffer.numStops; i++) {
            // Formatting has to be optimized
            long timeMs = (long) xStopsBuffer.stops[i] * 1000;
            String dateLabel = dateFormat.format(timeMs);

            canvas.drawText(
                    dateLabel,
                    axisXPositionsBuffer[i],
                    contentRect.bottom + labelHeight + labelSeparation,
                    labelTextPaint);
        }

        // Draws Y labels
        labelTextPaint.setTextAlign(Paint.Align.RIGHT);
        for (i = 0; i < yStopsBuffer.numStops; i++) {
            labelLength = formatFloat(labelBuffer, yStopsBuffer.stops[i], yStopsBuffer.decimals);
            labelOffset = labelBuffer.length - labelLength;
            canvas.drawText(
                    labelBuffer, labelOffset, labelLength,
                    contentRect.left - labelSeparation,
                    axisYPositionsBuffer[i] + labelHeight / 2,
                    labelTextPaint);
        }
    }

    /**
     * Rounds the given number to the given number of significant digits. Based on an answer on
     * <a href="http://stackoverflow.com/questions/202302">Stack Overflow</a>.
     */
    private static float roundToOneSignificantFigure(double num) {
        final float d = (float) Math.ceil((float) Math.log10(num < 0 ? -num : num));
        final int power = 1 - (int) d;
        final float magnitude = (float) Math.pow(10, power);
        final long shifted = Math.round(num * magnitude);
        return shifted / magnitude;
    }

    private static final int POW10[] = {1, 10, 100, 1000, 10000, 100000, 1000000};

    /**
     * Formats a float value to the given number of decimals. Returns the length of the string.
     * The string begins at out.length - [return value].
     */
    private static int formatFloat(final char[] out, float val, int digits) {
        boolean negative = false;
        if (val == 0) {
            out[out.length - 1] = '0';
            return 1;
        }
        if (val < 0) {
            negative = true;
            val = -val;
        }
        if (digits > POW10.length) {
            digits = POW10.length - 1;
        }
        val *= POW10[digits];
        long lval = Math.round(val);
        int index = out.length - 1;
        int charCount = 0;
        while (lval != 0 || charCount < (digits + 1)) {
            int digit = (int) (lval % 10);
            lval = lval / 10;
            out[index--] = (char) (digit + '0');
            charCount++;
            if (charCount == digits) {
                out[index--] = '.';
                charCount++;
            }
        }
        if (negative) {
            out[index--] = '-';
            charCount++;
        }
        return charCount;
    }

    /**
     * Computes the set of axis labels to show given start and stop boundaries and an ideal number
     * of stops between these boundaries.
     *
     * @param start    The minimum extreme (e.g. the left edge) for the axis.
     * @param stop     The maximum extreme (e.g. the right edge) for the axis.
     * @param steps    The ideal number of stops to create. This should be based on available screen
     *                 space; the more space there is, the more stops should be shown.
     * @param outStops The destination {@link AxisStops} object to populate.
     */
    private static void computeAxisStops(float start, float stop, int steps, AxisStops outStops) {
        double range = stop - start;
        if (steps == 0 || range <= 0) {
            outStops.stops = new float[]{};
            outStops.numStops = 0;
            return;
        }

        double rawInterval = range / steps;
        double interval = roundToOneSignificantFigure(rawInterval);
        double intervalMagnitude = Math.pow(10, (int) Math.log10(interval));
        int intervalSigDigit = (int) (interval / intervalMagnitude);
        if (intervalSigDigit > 5) {
            // Use one order of magnitude higher, to avoid intervals like 0.9 or 90
            interval = Math.floor(10 * intervalMagnitude);
        }

        double first = Math.ceil(start / interval) * interval;
        double last = Math.nextUp(Math.floor(stop / interval) * interval);

        double f;
        int i;
        int n = 0;
        for (f = first; f <= last; f += interval) {
            ++n;
        }

        outStops.numStops = n;

        if (outStops.stops.length < n) {
            // Ensure stops contains at least numStops elements.
            outStops.stops = new float[n];
        }

        for (f = first, i = 0; i < n; f += interval, ++i) {
            outStops.stops[i] = (float) f;
        }

        if (interval < 1) {
            outStops.decimals = (int) Math.ceil(-Math.log10(interval));
        } else {
            outStops.decimals = 0;
        }
    }

    /**
     * Draws the currently visible portion of the data series.
     * This method does not clip its drawing, so users should call {@link Canvas#clipRect
     * before calling this method.
     */
    private void drawGraphUnclipped(Canvas canvas) {
        ArrayList<MarketValue> marketValues = market.getValues();

        float x = contentRect.left;
        float y = getDrawY((float) marketValues.get(0).getPrice());

        graphLinesBuffer[0] = x;
        graphLinesBuffer[1] = y;
        graphLinesBuffer[2] = x;
        graphLinesBuffer[3] = y;

        for (int i = 1; i < marketValues.size(); i++) {
            MarketValue value = marketValues.get(i);

            // Set to previous two values
            graphLinesBuffer[i * 4 + 0] = graphLinesBuffer[(i - 1) * 4 + 2];
            graphLinesBuffer[i * 4 + 1] = graphLinesBuffer[(i - 1) * 4 + 3];

            x = value.getTime();
            y = (float) value.getPrice();

            graphLinesBuffer[i * 4 + 2] = getDrawX(x);
            graphLinesBuffer[i * 4 + 3] = getDrawY(y);
        }

        canvas.drawLines(graphLinesBuffer, 0, graphLinesBuffer.length, dataPaint);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //     Methods related to custom attributes
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////

    public float getLabelTextSize() {
        return labelTextSize;
    }

    public void setLabelTextSize(float labelTextSize) {
        this.labelTextSize = labelTextSize;
        initPaints();
        ViewCompat.postInvalidateOnAnimation(this);
    }

    public int getLabelTextColor() {
        return labelTextColor;
    }

    public void setLabelTextColor(int labelTextColor) {
        this.labelTextColor = labelTextColor;
        initPaints();
        ViewCompat.postInvalidateOnAnimation(this);
    }

    public float getGridThickness() {
        return gridThickness;
    }

    public void setGridThickness(float gridThickness) {
        this.gridThickness = gridThickness;
        initPaints();
        ViewCompat.postInvalidateOnAnimation(this);
    }

    public int getGridColor() {
        return gridColor;
    }

    public void setGridColor(int gridColor) {
        this.gridColor = gridColor;
        initPaints();
        ViewCompat.postInvalidateOnAnimation(this);
    }

    public float getAxisThickness() {
        return axisThickness;
    }

    public void setAxisThickness(float axisThickness) {
        this.axisThickness = axisThickness;
        initPaints();
        ViewCompat.postInvalidateOnAnimation(this);
    }

    public int getAxisColor() {
        return axisColor;
    }

    public void setAxisColor(int axisColor) {
        this.axisColor = axisColor;
        initPaints();
        ViewCompat.postInvalidateOnAnimation(this);
    }

    public float getDataThickness() {
        return dataThickness;
    }

    public void setDataThickness(float dataThickness) {
        this.dataThickness = dataThickness;
    }

    public int getDataColor() {
        return dataColor;
    }

    public void setDataColor(int dataColor) {
        this.dataColor = dataColor;
    }

    /**
     * A simple class representing axis label values.
     *
     * @see #computeAxisStops
     */
    private static class AxisStops {
        float[] stops = new float[]{};
        int numStops;
        int decimals;
    }

}
