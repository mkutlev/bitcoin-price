package mk.bitcoin.injection.module;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import mk.bitcoin.BitcoinApplication;
import mk.bitcoin.data.remote.ApiService;
import mk.bitcoin.injection.ApplicationContext;

/**
 * Provide application-level dependencies.
 */
@Module
public class ApplicationModule {

    protected final BitcoinApplication application;

    public ApplicationModule(BitcoinApplication application) {
        this.application = application;
    }

    @Provides
    BitcoinApplication provideApplication() {
        return application;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return application;
    }

    @Provides
    @Singleton
    ApiService provideApiService() {
        return ApiService.Factory.makeApiService();
    }


}
