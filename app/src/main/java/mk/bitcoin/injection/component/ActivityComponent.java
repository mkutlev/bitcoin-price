package mk.bitcoin.injection.component;

import dagger.Component;
import mk.bitcoin.injection.PerActivity;
import mk.bitcoin.injection.module.ActivityModule;
import mk.bitcoin.ui.bitcoin.BitcoinActivity;

/**
 * This component inject dependencies to all Activities across the application
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(BitcoinActivity activity);

}
