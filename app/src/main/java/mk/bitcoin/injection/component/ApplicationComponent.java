package mk.bitcoin.injection.component;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import mk.bitcoin.BitcoinApplication;
import mk.bitcoin.data.DataManager;
import mk.bitcoin.injection.ApplicationContext;
import mk.bitcoin.injection.module.ApplicationModule;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    @ApplicationContext
    Context getContext();

    BitcoinApplication getApplication();

    DataManager getDataManager();

    // Inject the getApplication
    void inject(BitcoinApplication application);

}
